#!/bin/sh

echo "<html><head><meta http-equiv='refresh' content=\"$delay; url='$destination'\"></head><body><p>If the redirection didn't happen, please click <a href='$destination'>this</a> link.</p></body></html>" > /usr/share/nginx/html/index.html

nginx-debug -g 'daemon off;'
