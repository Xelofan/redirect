FROM nginx:stable-alpine

WORKDIR /usr/share/nginx/html

ENV destination=exmaple.com, delay=0

RUN touch index.html

COPY ./script.sh /script.sh
RUN chmod +x /script.sh

CMD [ "sh", "/script.sh" ]

EXPOSE 80
